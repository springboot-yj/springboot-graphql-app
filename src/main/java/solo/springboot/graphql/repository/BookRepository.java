package solo.springboot.graphql.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import solo.springboot.graphql.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	
}
