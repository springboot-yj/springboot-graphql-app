# Springboot Graphql App


sing graphql-cli is now the recommended workflow to get and update your schema.

The following commands will get you started:

# install via NPM
npm install -g graphql-cli

# Setup your .graphqlconfig file (configure endpoints + schema path)
graphql init

# Download the schema from the server
graphql get-schema

sing graphql-cli is now the recommended workflow to get and update your schema.

The following commands will get you started:

# install via NPM
npm install -g graphql-cli

# Setup your .graphqlconfig file (configure endpoints + schema path)
graphql init

# Download the schema from the server
graphql get-schema


In case you just want to download the GraphQL schema, use the following approach:

The easiest way to get a GraphQL schema is using the CLI tool get-graphql-schema.

You can install it via NPM:

npm install -g get-graphql-schema


There are two ways to get your schema. 1) GraphQL IDL format or 2) JSON introspection query format.

GraphQL IDL format
get-graphql-schema ENDPOINT_URL > schema.graphql
JSON introspection format
get-graphql-schema ENDPOINT_URL --json > schema.json

