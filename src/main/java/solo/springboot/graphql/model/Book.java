package solo.springboot.graphql.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity
public class Book {

	@Id
//    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String title;
    private String isbn;
    private int pageCount;

    public Book() {
    }
    
    

	public Book(Long id, String title, String isbn, int pageCount) {
		super();
		this.id = id;
		this.title = title;
		this.isbn = isbn;
		this.pageCount = pageCount;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

}
