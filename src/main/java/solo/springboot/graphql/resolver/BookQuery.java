package solo.springboot.graphql.resolver;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import solo.springboot.graphql.model.Book;
import solo.springboot.graphql.repository.BookRepository;

@Component
public class BookQuery implements GraphQLQueryResolver {
	private static Logger logger = LoggerFactory.getLogger(BookQuery.class);

	@Autowired
	private BookRepository bookRepository;

	public List<Book> listAllBooks() {
		return bookRepository.findAll();
	}

	public Optional<Book> getBook(final Long id) {
		return bookRepository.findById(id);
	}

	@PostConstruct
	private void loadSchema() throws IOException {
		logger.info("Entering loadSchema@GraphQLService");
		loadDataIntoHSQL();

	}

	private void loadDataIntoHSQL() {
		
		Stream.of(
				new Book(new Long(1001), "The C Programming Language", "isbn_1001", 500),
				new Book(new Long("1002"), "Your Guide To Scrivener", "isbn_1002", 235),
				new Book(new Long("1003"), "Beyond the Inbox: The Power User Guide to Gmail", "isbn_1003",320),
				new Book(new Long("1004"), "Scratch 2.0 Programming", "isbn_1004", 450),
				new Book(new Long("1005"), "Pro Git", "isbn_1005", 119)

		).forEach(book -> {
			bookRepository.save(book);
		});
	}
}
